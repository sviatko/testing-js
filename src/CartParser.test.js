import CartParser from "./CartParser";

const STRING_TO_PARSE = "Mollis consequat,9.00,2";
const PARSED_OBJECT = {
  id: "3e6def17-5e87-4f27-b6b8-ae78948523a9",
  name: "Mollis consequat",
  price: 9,
  quantity: 2,
};

let parser, validate, parseLine, readFile, errorType, parse;
beforeEach(() => {
  parser = new CartParser();

  errorType = parser.ErrorType;

  validate = parser.validate.bind(parser);
  parseLine = parser.parseLine.bind(parser);
  parse = parser.parse.bind(parser);
  readFile = parser.readFile.bind(parser);
});

describe("CartParser - unit tests", () => {
  describe("parseLine", () => {
    it("should match all properties from associated line", () => {
      Object.keys(PARSED_OBJECT).map(
        (key) =>
          key !== "id" &&
          expect(parseLine(STRING_TO_PARSE)[key]).toBe(PARSED_OBJECT[key])
      );
    });

    it("should match all properties and value type from associated line", () => {
      Object.keys(PARSED_OBJECT).map((key) => {
        if (key !== "id") {
          expect(parseLine(STRING_TO_PARSE)[key]).toBe(PARSED_OBJECT[key]);
          expect(typeof parseLine(STRING_TO_PARSE)[key]).toBe(
            typeof PARSED_OBJECT[key]
          );
        }
      });
    });
  });

  describe("validate", () => {
    it("should return 0", () => {
      const correctContents = readFile(__dirname + "/correct.csv");

      expect(validate(correctContents).length).toBe(0);
    });

    it("should return headers error", () => {
      const withoutHeadersContents = readFile(
        __dirname + "/withoutHeaders.csv"
      );

      expect(validate(withoutHeadersContents)[0].type).toBe(errorType.HEADER);
    });

    it("should return body error", () => {
      const withWrongTotalOfCells = readFile(
        __dirname + "/withWrongTotalOfCells.csv"
      );

      expect(validate(withWrongTotalOfCells)[0].type).toBe(errorType.ROW);
      expect(validate(withWrongTotalOfCells)[0].message).toContain(
        "Expected row to have 3 cells but received 2."
      );
    });

    it("should return cell is empty error", () => {
      const cellHasEmptyString = readFile(
        __dirname + "/cellHasEmptyString.csv"
      );

      expect(validate(cellHasEmptyString)[0].type).toBe(errorType.CELL);
      expect(validate(cellHasEmptyString)[0].message).toContain(
        "nonempty string"
      );
    });

    it("should return cell is empty, positive or not a number error. Second value has to be a positive number", () => {
      const secondCellHasNotNumberValue = readFile(
        __dirname + "/secondCellHasNotNumberValue.csv"
      );

      expect(validate(secondCellHasNotNumberValue)[0].type).toBe(
        errorType.CELL
      );
      expect(validate(secondCellHasNotNumberValue)[0].message).toContain(
        "positive number"
      );
    });

    it("should return cell is empty, positive or not a number error. Third value has to be a positive number", () => {
      const thirdCellHasNotNumberValue = readFile(
        __dirname + "/thirdCellHasNotNumberValue.csv"
      );

      expect(validate(thirdCellHasNotNumberValue)[0].type).toBe(errorType.CELL);
      expect(validate(thirdCellHasNotNumberValue)[0].message).toContain(
        "positive number"
      );
    });

    it("should return total price equal to 348.32", () => {
      const path = __dirname + "/correct.csv";

      expect(parse(path).total).toEqual(348.32);
    });

    test("should throw an Error", () => {
      const path = __dirname + "/cellHasEmptyString.csv";

      try {
        expect(parse(path)).toThrow();
      } catch (e) {
        expect(e.message).toBe("Validation failed!");
      }
    });
  });
});

describe("CartParser - integration test", () => {
  // Add your integration test here.
});
